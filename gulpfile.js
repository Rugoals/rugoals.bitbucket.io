var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-minify-css');
var browserSync = require("browser-sync");
var htmlInjector = require("bs-html-injector");

// server start
function server(done) {
    browserSync.use(htmlInjector, {
        files: "*.html"
    });
    browserSync.init({
        server: "./",
        notify: true
    });
    done();
}

gulp.task('sass', function() {
    return gulp.src('style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(minifyCSS({
            keepBreaks: true
        }))
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.stream());
});

function watch() { 
    gulp.watch('*.scss', gulp.parallel('sass'));
}

gulp.task("default",server );//gulp.series(server, watch)